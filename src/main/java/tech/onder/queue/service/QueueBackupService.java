package tech.onder.queue.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.base.Strings;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.libs.Json;
import tech.onder.queue.IRestorableStorage;
import tech.onder.queue.conf.ConsumptionQueueConf;
import tech.onder.queue.models.ConsumptionChunk;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class QueueBackupService {
    
    private final Logger logger = LoggerFactory.getLogger(QueueBackupService.class);
    
    private IRestorableStorage<ConsumptionChunk> iRestorableStorage;
    
    private ConsumptionQueueConf consumptionQueueConf;
    
    @Inject
    public QueueBackupService(IRestorableStorage<ConsumptionChunk> iRestorableStorage, ConsumptionQueueConf aConsumptionQueueConf) {
        this.iRestorableStorage = iRestorableStorage;
        this.consumptionQueueConf = aConsumptionQueueConf;
    }
    
    public CompletionStage<Void> backup() {
        return CompletableFuture.runAsync(this::backupMeters);
        
    }
    
    private void backupMeters() {
        try {
            File fw = new File(consumptionQueueConf.getBackupFilename());
            List<ConsumptionChunk> chunkToStore = iRestorableStorage.dump();
            String json = Json.toJson(chunkToStore).toString();
            FileUtils.writeStringToFile(fw, json);
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }
    
    public CompletableFuture<Void> restore() {
        return CompletableFuture.runAsync(this::loadMeters);
    }
    
    
    private void loadMeters() {
        try {
            File fw = new File(consumptionQueueConf.getBackupFilename());
            String meterStorageContent = FileUtils.readFileToString(fw);
            if (Strings.isNullOrEmpty(meterStorageContent)) {
                logger.trace("Empty backup file");
                return;
            }
            JsonNode jsonOfMeters = Json.parse(meterStorageContent);
            ConsumptionChunk[] chunkReports = Json.fromJson(jsonOfMeters, ConsumptionChunk[].class);
            iRestorableStorage.loadAll(Arrays.asList(chunkReports));
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }
    
}
