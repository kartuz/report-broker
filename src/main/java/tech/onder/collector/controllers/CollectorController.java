package tech.onder.collector.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import play.mvc.StatusHeader;
import tech.onder.collector.models.dto.TransactionInputDTO;
import tech.onder.collector.services.impl.CollectorService;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class CollectorController extends Controller {
    
    private final Logger logger = LoggerFactory.getLogger(CollectorController.class);
    
    private final HttpExecutionContext ec;
    
    private final CollectorService collectorService;
    
    @Inject
    public CollectorController(HttpExecutionContext ec, CollectorService collectorService) {
        this.ec = ec;
        this.collectorService = collectorService;
    }
    
    public CompletionStage<Result> push(String uuid) {
        return CompletableFuture.supplyAsync(() -> ctx().request().body().asJson(), ec.current())
                                .thenApply(jsonNode -> Json.fromJson(jsonNode, TransactionInputDTO.class))
                                .thenAccept(dto -> collectorService.add(uuid, dto))
                                .thenApply((v) -> Results.ok());
    }
    
    /**
     * @return Loads data from log file.
     * This method does not guarantee anything.
     */
    @Deprecated
    public CompletionStage<Result> pushList() {
        return CompletableFuture.supplyAsync(() -> {
                    try {
                        logger.info("load");
                        ObjectMapper mapper = new ObjectMapper();
                        List<?> meterStorage = FileUtils.readLines(new File("/home/ubuntu/report-broker/conf/meters-storage-back.json"));
                        // List<?> meterStorage = FileUtils.readLines(new File("conf/meters-storage-back.json"));
                        meterStorage.stream()
                                    .map(s -> {
                                        try {
                                            return mapper.readValue((String) s, TransactionInputDTO.class);
                                        } catch (IOException ioe) {
                                            return null;
                                        }
                            
                                    })
                                    .filter(Objects::nonNull)
                                    .forEach(dto -> collectorService.add(dto.getBuyerId(), dto));
                        return Results.ok();
                    } catch (IOException ioe) {
                        return Results.badRequest();
                    }
                    
                }
                , ec.current());
    }
    
}
